import { useEffect, useState } from "react";

function Count() {
    const [count,setCount] = useState(0); 
    const increaseCout = () => {
        console.log("increase count");
        setCount(count + 1);
    }
    useEffect(() => {
        console.log("componentDIdUpdate!");
        document.title = `You clicked ${count} times!`
        return () => {
            console.log("componentWillmount")
        }
    })

    useEffect(() => {
        console.log("componentDIdMount!");
        document.title = `You clicked ${count} times!`
    },[]) 
    return (
        <>
            <p>You clicked {count} times!</p>
            <button onClick={increaseCout}>CLick me!</button>
        </>
    )
}

export default Count;